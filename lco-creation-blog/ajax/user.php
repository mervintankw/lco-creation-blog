<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    user.php
 * Date Created: Apr 11, 2015
 * Time Created: 4:00:26 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require("../class/user.php");
require("../class/syscon.php");
$html = new html();
$user = new user();
$syscon = new syscon();

switch($_POST["mode"])
{
    case("delete"):
        $aB010 = $user->getUserList();
        $modalArr = $html->generateModal("deleteModal","Delete User",ROOTURI."/controller/user.php","delete"); 
        ?>
        <?=$modalArr["header"];?>
        <table id="delUserTable" class='table table-responsive'>
            <thead>
                <tr>
                    <th></th>
                    <th>User Name</th>
                    <th>Person Name</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($aB010 as $B010){ ?>
                <tr class="table-row" style="cursor: pointer;" data-id="<?=$B010["B010UserCd"];?>">
                    <td><?=$html->generateInput(" name='del[]' type='checkbox' ",$B010["B010UserCd"])?></td>
                    <td><?=$B010["B010UserNm"];?></td>
                    <td><?=$B010["B010Nm"];?></td>
                    <td><?=$B010["B010Status"];?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?=$modalArr["footer"];?>
        <script>
            $(document).ready(function(){
                $("#delUserTable").dataTable();
            });
        </script>
        <?php
        break;
    case("entry"):
        $B010UserCd = $_POST["id"];
        $mode = empty($B010UserCd)?"new":"edit";
        $aB010 = array();
        if($mode=="new")
        {
            $B010 = array();
        }
        else
        {
            $aB010 = $user->getUserList($B010UserCd);
            // Get user data
            $B010 = $aB010[0];
        }
        // Get user types
        $aUserType = $syscon->getUserType();
        $UserType = empty($aUserType)?array():$aUserType;
        // Get user status
        $UserStatus = array("Active"=>"Active","Disabled"=>"Disabled");
        
        $modalArr = $html->generateModal("entryModal",ucfirst($mode)." User",ROOTURI."/controller/user.php",$mode);
        ?>
        <?=$modalArr["header"];?>
        <div class="form-group">
            <label class="control-label col-sm-2">Profile Pic</label>
            <div class="col-sm-6">
                <div style="width:200px;height:200px;background:url('<?=ROOTURI.@$B010["B010ProfilePic"];?>');background-size:cover;"></div>
                <?=$html->generateInput(" name='B010ProfilePic' type='file' class='form-control' ",@$B010["B010ProfilePic"]);?>
                <?=$html->generateInput(" name='B010ProfilePicHd' type='hidden' ",@$B010["B010ProfilePic"]);?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">User Code</label>
            <div class="col-sm-3">
                <?=$html->generateInput(" name='B010UserCd' type='text' class='form-control' readonly ",@$B010["B010UserCd"]);?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">User Type</label>
            <div class="col-sm-3">
                <?=$html->generateSelect(" name='B010Type' class='form-control' required ","array",$UserType,"Zsy99Descp1","Zsy99Key1","Select User Type",@$B010["B010Type"])?>
            </div>
            <label class="control-label col-sm-2">Status</label>
            <div class="col-sm-3">
                <?=$html->generateSelect(" name='B010Status' class='form-control' required ","array-single",$UserStatus,"","","Select User Status",@$B010["B010Status"])?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">User Name</label>
            <div class="col-sm-3">
                <?=$html->generateInput(" name='B010UserNm' type='text' class='form-control' required ",@$B010["B010UserNm"]);?>
            </div>
            <label class="control-label col-sm-2">Password</label>
            <div class="col-sm-3">
                <?=$html->generateInput(" name='B010Passwd' type='password' class='form-control' ");?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Name</label>
            <div class="col-sm-3">
                <?=$html->generateInput(" name='B010Nm' type='text' class='form-control' required ",@$B010["B010Nm"]);?>
            </div>
            <label class="control-label col-sm-2">Email</label>
            <div class="col-sm-4">
                <?=$html->generateInput(" name='B010Email' type='text' class='form-control' required ",@$B010["B010Email"]);?>
            </div>
        </div>
        <?=$modalArr["footer"];?>
        <script>
            
        </script>
        <?php
        break;
}