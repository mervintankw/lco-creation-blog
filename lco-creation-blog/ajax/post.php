<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    post.php
 * Date Created: Apr 6, 2015
 * Time Created: 10:59:52 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require("../class/post.php");
$html = new html();
$post = new post();

switch($_POST["mode"])
{
    case("loadMore"):
        $aB100 = $post->getPostByRange($_POST["start"],$_POST["end"]);
        ?>
        <?php foreach($aB100 as $B100){ ?>
            <article class="white-panel panel panel-default">
                <div class="panel-body">
                <h4><?=$B100["B100Title"];?></h4>
                <p><?=truncate($B100["B100Content"],200,array("html"=>true,"ending"=>' <a style="cursor: pointer;" onclick="readMore(\''.$B100["B100SeqNo"].'\');">Read More</a>'));?></p>
                </div>
                <div class="panel-footer text-right">
                        <?=formatDateTime($B100["B100CreDt"]);?>
                </div>
            </article>
        <?php } ?>
        <?php
        break;
    case("readMore"):
        $B100 = $post->getPost($_POST["id"])[0];
        $extraBtnArr = array('<button type="button" class="btn btn-default" data-dismiss="modal">Done Reading</button>');
        $modalArr = $html->generateModal("readMoreModal",$B100["B100Title"],"","",$extraBtnArr,false);
        ?>
        <?=$modalArr["header"];?>
        <div class="row form-group">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><?=nl2br($B100["B100Content"]);?></p>
                    </div>
                    <div class="panel-footer text-right">
                        <?=formatDateTime($B100["B100CreDt"]);?>
                    </div>
                </div>
            </div>
        </div>
        <?=$modalArr["footer"];?>
        <?php
        break;
    case("archive"):
        $aB100 = $post->getPost();
        $modalArr = $html->generateModal("archiveModal","Archive Post",ROOTURI."/controller/post.php","archive"); 
        ?>
        <?=$modalArr["header"];?>
        <table id="archivePostTable" class='table table-responsive'>
            <thead>
                <tr>
                    <th></th>
                    <th>Post ID</th>
                    <th>Title</th>
                    <th>Modified Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($aB100 as $B100){ ?>
                <tr class="table-row" style="cursor: pointer;" data-id="<?=$B100["B100SeqNo"];?>">
                    <td><?=$html->generateInput(" name=arch[]' type='checkbox' ",$B100["B100SeqNo"])?></td>
                    <td><?=$B100["B100SeqNo"];?></td>
                    <td><?=$B100["B100Title"];?></td>
                    <td><?=$B100["B100ModDt"];?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?=$modalArr["footer"];?>
        <script>
            $(document).ready(function(){
                $("#archivePostTable").dataTable();
            });
        </script>
        <?php
        break;
    case("delete"):
        $aB100 = $post->getArchivedPost();
        $extraBtnArr = array('<button type="submit" class="btn btn-info" name="restore">Restore</button>');
        $modalArr = $html->generateModal("deleteModal","Delete Post",ROOTURI."/controller/post.php","delete",$extraBtnArr); 
        ?>
        <?=$modalArr["header"];?>
        <table id="delPostTable" class='table table-responsive'>
            <thead>
                <tr>
                    <th></th>
                    <th>Post ID</th>
                    <th>Title</th>
                    <th>Modified Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($aB100 as $B100){ ?>
                <tr class="table-row" style="cursor: pointer;" data-id="<?=$B100["B100SeqNo"];?>">
                    <td><?=$html->generateInput(" name='del[]' type='checkbox' ",$B100["B100SeqNo"])?></td>
                    <td><?=$B100["B100SeqNo"];?></td>
                    <td><?=$B100["B100Title"];?></td>
                    <td><?=$B100["B100ModDt"];?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?=$modalArr["footer"];?>
        <script>
            $(document).ready(function(){
                $("#delPostTable").dataTable();
            });
        </script>
        <?php
        break;
    case("entry"):
        $B100SeqNo = $_POST["id"];
        $mode = empty($B100SeqNo)?"new":"edit";
        if($mode=="new")
        {
            $B100 = array();
        }
        else
        {
            $B100 = $post->getPost($B100SeqNo)[0];
        }
        
        $PostType = array("Image"=>"Image","Text"=>"Text");
        
        $modalArr = $html->generateModal("entryModal",ucfirst($mode)." Post",ROOTURI."/controller/post.php",$mode);
        ?>
        <?=$modalArr["header"];?>
        <div class="form-group">
            <label class="control-label col-sm-2">Post ID</label>
            <div class="col-sm-3">
                <?=$html->generateInput(" name='B100SeqNo' type='text' class='form-control' readonly ",@$B100["B100SeqNo"]);?>
            </div>
            <!--<label class="control-label col-sm-2">Post Type</label>-->
            <!--<div class="col-sm-3">-->
                <?//=$html->generateSelect(" name='B100Type' class='form-control' required ","array",$PostType,"","","Select Post Type",@$B100["B100Type"]);?>
            <!--</div>-->
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Post Title</label>
            <div class="col-sm-9">
                <?=$html->generateInput(" name='B100Title' type='text' class='form-control' required ",@$B100["B100Title"]);?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Post Content</label>
            <div class="col-sm-9">
                <?=$html->generateTextArea(" name='B100Content' style='height:300px;' ",@nl2br($B100["B100Content"]));?>
            </div>
        </div>
        <?=$modalArr["footer"];?>
        <script>
            $(document).ready(function(){
                tinymce.remove();
                tinymce.init({
                    selector: "textarea",
                    relative_urls: false,
                    plugins: "image"
                });
            });
        </script>
        <?php
        break;
}