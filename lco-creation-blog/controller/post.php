<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    post.php
 * Date Created: Apr 6, 2015
 * Time Created: 11:07:58 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require("../class/post.php");
$post = new post();

if(isset($_POST))
{
    $_POST = filter_input_array(INPUT_POST);
    
    if(isset($_POST["delete"]))
    {
        if(isset($_POST["del"]))
        {
            foreach($_POST["del"] as $index => $value)
            {
                $post->deletePost($value);
            }
        }
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/post.php';</script>";
    }
    
    if(isset($_POST["restore"]))
    {
        if(isset($_POST["del"]))
        {
            foreach($_POST["del"] as $index => $value)
            {
                $post->restorePost($value);
            }
        }
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/post.php';</script>";
    }
    
    if(isset($_POST["archive"]))
    {
        if(isset($_POST["arch"]))
        {
            foreach($_POST["arch"] as $index => $value)
            {
                $post->archivePost($value);
            }
        }
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/post.php';</script>";
    }
    
    if(isset($_POST["new"]))
    {
        $post->insertPost("Text",$_POST["B100Title"],$_POST["B100Content"]);
        echo "<script>window.parent.location.href='".ROOTURI."/view/post.php';</script>";
    }
    
    if(isset($_POST["edit"]))
    {
        $post->updatePost($_POST["B100SeqNo"],"Text",$_POST["B100Title"],$_POST["B100Content"]);
        echo "<script>window.parent.location.href='".ROOTURI."/view/post.php';</script>";
    }
}