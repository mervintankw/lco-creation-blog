<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    user.php
 * Date Created: Apr 11, 2015
 * Time Created: 4:25:21 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require("../class/user.php");
$user = new user();

if(isset($_POST))
{
    $_POST = filter_input_array(INPUT_POST);
    
    if(isset($_POST["delete"]))
    {
        if(isset($_POST["del"]))
        {
            foreach($_POST["del"] as $index => $value)
            {
                $user->deleteUser($value);
            }
        }
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/user.php';</script>";
    }
    
    if(isset($_POST["new"]))
    {
        $imagePath = isset($_FILES["B010ProfilePic"])?uploadImage("B010ProfilePic"):"";
        $user->addUser(
                $_POST["B010Type"],
                $_POST["B010UserNm"],
                $_POST["B010Passwd"],
                $_POST["B010Nm"],
                $_POST["B010Email"],
                $imagePath,
                $_POST["B010Status"],
                $_SESSION["UserCd"],
                $_SESSION["UserCd"]
        );
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/user.php';</script>";
    }
    
    if(isset($_POST["edit"]))
    {
        $imagePath = isset($_FILES["B010ProfilePic"])?uploadImage("B010ProfilePic"):$_POST["B010ProfilePic"];
        $imagePath = empty($imagePath)?$_POST["B010ProfilePicHd"]:$imagePath;
        $user->updateUser(
                $_POST["B010UserCd"],
                $_POST["B010Type"],
                $_POST["B010UserNm"],
                $_POST["B010Passwd"],
                $_POST["B010Nm"],
                $_POST["B010Email"],
                $imagePath,
                $_POST["B010Status"],
                $_SESSION["UserCd"]
        );
        
        echo "<script>window.parent.location.href='".ROOTURI."/view/user.php';</script>";
    }
}