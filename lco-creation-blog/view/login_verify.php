<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    login_verify.php
 * Date Created: Mar 23, 2015
 * Time Created: 3:50:19 AM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");

include(ROOTPATH."/class/user.php");

$db = new db();
$user = new user();

if(isset($_POST["username"]) && isset($_POST["password"]))
{
    $aB010 = $user->login($_POST["username"],$_POST["password"]);
    if(!empty($aB010))
    {
        $B010 = $aB010[0];
        $_SESSION["UserCd"]             = $B010["B010UserCd"];
        $_SESSION["UserNm"]             = $B010["B010UserNm"];
        $_SESSION["Email"]              = $B010["B010Email"];
        $_SESSION["CompanyID"]          = $B010["B010CompanyID"];
        echo "<script type='text/javascript'>top.location.href='/view/dashboard.php';</script>";
    }
    else
    {
        echo "<script type='text/javascript'>alert('Invalid login. Please try again.');</script>";
        echo "<script type='text/javascript'>top.location.href='/view/login.php';</script>";
        exit;
    }
}