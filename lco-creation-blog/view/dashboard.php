<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    dashboard.php
 * Date Created: Apr 5, 2015
 * Time Created: 3:40:09 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require(ROOTPATH."/class/template.php");
require(ROOTPATH."/class/permission.php");
require(ROOTPATH."/class/post.php");
$permission = new permission();
$template = new template();
$post = new post();
$template->header();

$aB100 = $post->getPostStats();
?> 
<?php $template->menu();?>
<div class="container">
    <h2>Dashboard</h2><br>
    <h4>Posts Per Month</h4>
    <div id="graph"></div>
<pre id="code" class="prettyprint linenums hidden">
// Use Morris.Bar
Morris.Bar({
  element: 'graph',
  data: [
<?php foreach($aB100 as $B100){ ?>
    {x: '<?=$B100["monthYear"]?>', y: <?=$B100["numPerMonth"]?>},
<?php } ?>
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(255 * row.y / this.ymax);
      return 'rgb(' + red + ',0,0)';
    }
    else {
      return '#000';
    }
  }
});
</pre>
</div>
<?php $template->scripts();?>
<script>
    $(function(){
        eval($('#code').text());
        prettyPrint();
    });
</script>
<?php $template->footer();