<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    post.php
 * Date Created: Apr 6, 2015
 * Time Created: 10:24:35 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require(ROOTPATH."/class/template.php");
require(ROOTPATH."/class/post.php");
require(ROOTPATH."/class/permission.php");
$permission = new permission();
$template = new template();
$post = new post();
$template->header();
$aB100 = $post->getPost();
?> 
<?php $template->menu();?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-6"><h2>Posts</h2></div>
        <div class="col-md-6">
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-default btn-sm btn-success" id="new">New </a>
                <a href="#" class="btn btn-default btn-sm btn-warning" id="archive">Archive</a>
                <a href="#" class="btn btn-default btn-sm btn-danger" id="del">Delete</a>
            </div>
        </div>
    </div>
    <div class="blank-space"></div>
    <table id="postTable" class='table table-responsive'>
        <thead>
            <tr>
                <th>Post ID</th>
                <th>Title</th>
                <th>Created Date</th>
                <th>Last Modified</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aB100 as $B100){ ?>
            <tr class="table-row" style="cursor: pointer;" data-id="<?=$B100["B100SeqNo"];?>">   
                <td><?=$B100["B100SeqNo"];?></td>
                <td><?=$B100["B100Title"];?></td>
                <td><?=formatDateTime($B100["B100CreDt"]);?></td>
                <td><?=formatDateTime($B100["B100ModDt"]);?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <div id="ajaxPanel"></div>
</div>
<?php $template->scripts();?>
<script>
    $(document).ready(function(){
        $("#postTable").dataTable({
            "language": {
                "processing": "DataTables is currently busy"
            }
         });
    });
    $('#del').click(function(){
        $.post("<?=ROOTURI;?>/ajax/post.php",
        {
            mode:"delete"
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#deleteModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
    $('#archive').click(function(){
        $.post("<?=ROOTURI;?>/ajax/post.php",
        {
            mode:"archive"
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#archiveModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
    $('#new').click(function(){
        $.post("<?=ROOTURI;?>/ajax/post.php",
        {
            mode:"entry",
            id:""
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#entryModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
    $('.table-row').click(function(){
        $.post("<?=ROOTURI;?>/ajax/post.php",
        {
          mode:"entry",
          id:$(this).closest("tr").data('id')
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#entryModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
</script>
<?php $template->footer();