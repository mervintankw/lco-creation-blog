<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    logout.php
 * Date Created: Mar 25, 2015
 * Time Created: 6:26:00 PM
 */

session_start();
session_unset();
session_destroy();

echo "<script type='text/javascript'>top.location.href='/view/login.php';</script>";
exit;