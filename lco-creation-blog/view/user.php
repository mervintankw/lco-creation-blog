<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    user.php
 * Date Created: Apr 11, 2015
 * Time Created: 3:59:24 PM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
require(ROOTPATH."/class/template.php");
require(ROOTPATH."/class/user.php");
require(ROOTPATH."/class/permission.php");
$permission = new permission();
$template = new template();
$user = new user();
$template->header();
$aB010 = $user->getUserList();
$template->menu();
?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-6"><h2>Users</h2></div>
        <div class="col-md-6">
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-default btn-sm btn-success" id="new">New </a>
                <a href="#" class="btn btn-default btn-sm btn-danger" id="del">Delete</a>
            </div>
        </div>
    </div>
    <div class="blank-space"></div>
    <table id="userTable" class='table table-responsive'>
        <thead>
            <tr>
                <th>User Name</th>
                <th>Person Name</th>
                <th>Email</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($aB010 as $B010){ ?>
            <tr class="table-row" style="cursor: pointer;" data-id="<?=$B010["B010UserCd"];?>">   
                <td><?=$B010["B010UserNm"];?></td>
                <td><?=$B010["B010Nm"];?></td>
                <td><?=$B010["B010Email"];?></td>
                <td><?=$B010["B010Status"];?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <div id="ajaxPanel"></div>
</div><!-- ./Modal HTML -->
<?php $template->scripts();?>
<script>
    $(document).ready(function(){
        $("#userTable").dataTable();
    });
    $('#del').click(function(){
        $.post("<?=ROOTURI;?>/ajax/user.php",
        {
            mode:"delete"
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#deleteModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
    $('#new').click(function(){
        $.post("<?=ROOTURI;?>/ajax/user.php",
        {
            mode:"entry",
            id:""
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#entryModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
    $('.table-row').click(function(){
        $.post("<?=ROOTURI;?>/ajax/user.php",
        {
          mode:"entry",
          id:$(this).closest("tr").data('id')
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#entryModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    });
</script>
<?php $template->footer();?>