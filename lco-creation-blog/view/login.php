<?php
/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    login.php
 * Date Created: Mar 11, 2015
 * Time Created: 9:35:39 PM
 */
?>
<html>
    <head>
        <title>LCO-Creation Test Blog</title>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/login.css" rel="stylesheet" type="text/css"/>
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="jumbotron" style="height:400px;">
            <div class="container">
                <!--<span class="glyphicon glyphicon-list-alt"></span>-->
                <a href="../index.php"><img src="../img/bt_logo.png" alt="" style="width:100%;"/></a>
                <!--<span style="font-family:Roboto;font-size:30px;" class="">LCO-Creation<br>Test Blog</span>-->
                <!--<h2>Login</h2>-->
                <div class="box">
                    <form name="loginForm" method="POST" action="login_verify.php">
                        <input type="text" placeholder="username" name="username">
                        <input type="password" placeholder="password" name="password">
                        <input type="submit" class="btn btn-default full-width" value="Login"><i style="color:#fff;" class="fa fa-sign-in fa-2x"></i>
                    </form>
                </div>
            </div>
        </div>
        <script src="../js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>