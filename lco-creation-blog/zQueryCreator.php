<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    zQueryCreator.php
 * Date Created: Mar 23, 2015
 * Time Created: 3:37:28 AM
 */

require($_SERVER["DOCUMENT_ROOT"]."/common/include.php");
$db = new db();

if(isset($_GET["submit"]))
{
    $qF1 ="DESCRIBE ".$_GET["TableNm"];
    $rF1 = $db->db_query_table($qF1);
    $LF3 = "";
    switch($_GET["mode"])
    {
        case("Query"):
            $LF1 = "";
            $LF2 = "";

            $MaxFieldLength = 0;
            $FLn = 0;
            foreach($rF1 as $aF1)
            {
                $LF1 .= $aF1["Field"]." = '".'".'."$".$_GET["Variable"].'['.'"'.str_replace($_GET["From"],$_GET["To"],$aF1["Field"]).'"'.']'.'."'."'"." ".",<br/>";
                $LF2 .= $aF1["Field"]." = "."$".$_GET["Variable"].'".["'.str_replace($_GET["From"],$_GET["To"],$aF1["Field"]).'"]'.",<BR>";

                $LArr[] = $aF1["Field"];
                $MaxFieldLength = ($MaxFieldLength < strlen($aF1["Field"]))? strlen($aF1["Field"]):$MaxFieldLength;
            }

            foreach($LArr as $key=>$FieldNm)
            {
                $Spaces = "";
                for($i=0; $i<=($MaxFieldLength-strlen($FieldNm)); $i++)
                {
                    $Spaces .= "&nbsp;";
                }

                $LF3 .= $FieldNm.$Spaces." = '".'".'."$".$_GET["Variable"].'["'.str_replace($_GET["From"],$_GET["To"],$FieldNm).'"]'.'."'."'"." ".",<BR>";
            }
        break;
        case("FieldList"):
            foreach($rF1 as $aF1)
            {
                $LF3 .= $aF1["Field"].",";
            }
        break;
    }
}

?>
<html >
    <form name="Frm" method="get" target="_self">
        <table>
            <tr>
                <td>Mode:</td>
                <td>
                    <select name="mode">
                        <option value="Query">Query</option>
                        <option value="FieldList">FieldList</option>
                    </select>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>TableName:</td>
                <td><input type="text" name="TableNm" value="<?=@$_GET["TableNm"];?>" /></td>
            </tr>
            <tr>
                <td>From:</td>
                <td><input type="text" name="From" value="<?=@$_GET["From"];?>" /></td>
            </tr>
            <tr>
                <td>To:</td>
                <td><input type="text" name="To" value="<?=@$_GET["To"];?>" /></td>
            </tr>
            <tr>
                <td>Prefix:</td>
                <td><input type="text" name="Variable" value="<?=@$_GET["Variable"];?>" /></td>
            </tr>
        </table>
        <input type="submit" name="submit" value="submit" /></br>
        <div class="left" ><?=@$LF3;?></div>
    </form>
</html>