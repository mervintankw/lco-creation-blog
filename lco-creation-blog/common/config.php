<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    config.php
 * Date Created: Mar 12, 2015
 * Time Created: 12:05:21 AM
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ENVIROMENT','Test');
define("ROOTURI","http://".$_SERVER["SERVER_NAME"]);
define("ROOTPATH",$_SERVER["DOCUMENT_ROOT"]);
define("SITE_ROOT_PATH",dirname(dirname(__FILE__)));
define("SCRIPT_NAME",ucfirst(basename($_SERVER["SCRIPT_FILENAME"],'.php')));

define("TITLE",'LCO-Creation Blog Test');

define("DB_DATABASE", "flyingsh_blogtest");
define("DB_SERVER_URL", "127.0.0.1:3306");
define("DB_USERNAME", "flyingsh_blogtes");
define("DB_PASSWORD", "13042015");
define("DB_SERVER_PORT", "3306");

define("SALT", "h234j20jrijdqwod2sddne");
define("SUGAR", "1241asfh24g2dadagfa");

define("LOAD_NUM_OF_POSTS", 5);