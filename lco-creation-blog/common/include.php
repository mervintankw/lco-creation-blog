<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    include.php
 * Date Created: Mar 16, 2015
 * Time Created: 9:32:02 AM
 */

session_start();

date_default_timezone_set('Asia/Singapore');

require($_SERVER["DOCUMENT_ROOT"]."/common/config.php");
require(ROOTPATH."/common/common.php");

require(ROOTPATH."/class/html.php");
require(ROOTPATH."/class/db.php");
require(ROOTPATH."/class/main.php");