<?php

/**
 * For Testing Only
 *
 * print array
 */
function PArr($Arr) 
{
    echo "<pre>";
    print_r($Arr);
    echo "</pre>";
}

/**
 * CURL method to send GET request
 * @param string $url
 * @return string
 */
function httpGet($url) 
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}

/**
 * CURL method to send POST request
 * @param string $url
 * @param array $fields
 * @return string
 */
function httpPOST($url,$fields="")
{
    //url-ify the data for the POST
    $fields_string = "";
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string,'&');
    
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    //execute post
    $output = curl_exec($ch);
    
    // close curl connection
    curl_close($ch);
    
    return $output;
}

/**
 * function appends read more link if it is too long
 * @param string $text
 * @param int $length
 * @param array $options
 * @return string
 */
function truncate($text, $length = 100, $options = array()) {
    $default = array(
        'ending' => '...', 'exact' => true, 'html' => false
    );
    $options = array_merge($default, $options);
    extract($options);

    if ($html) {
        if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        $totalLength = mb_strlen(strip_tags($ending));
        $openTags = array();
        $truncate = '';

        preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
        foreach ($tags as $tag) {
            if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                    array_unshift($openTags, $tag[2]);
                } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                    $pos = array_search($closeTag[1], $openTags);
                    if ($pos !== false) {
                        array_splice($openTags, $pos, 1);
                    }
                }
            }
            $truncate .= $tag[1];

            $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
            if ($contentLength + $totalLength > $length) {
                $left = $length - $totalLength;
                $entitiesLength = 0;
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entitiesLength <= $left) {
                            $left--;
                            $entitiesLength += mb_strlen($entity[0]);
                        } else {
                            break;
                        }
                    }
                }

                $truncate .= mb_substr($tag[3], 0 , $left + $entitiesLength);
                break;
            } else {
                $truncate .= $tag[3];
                $totalLength += $contentLength;
            }
            if ($totalLength >= $length) {
                break;
            }
        }
    } else {
        if (mb_strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = mb_substr($text, 0, $length - mb_strlen($ending));
        }
    }
    if (!$exact) {
        $spacepos = mb_strrpos($truncate, ' ');
        if (isset($spacepos)) {
            if ($html) {
                $bits = mb_substr($truncate, $spacepos);
                preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                if (!empty($droppedTags)) {
                    foreach ($droppedTags as $closingTag) {
                        if (!in_array($closingTag[1], $openTags)) {
                            array_unshift($openTags, $closingTag[1]);
                        }
                    }
                }
            }
            $truncate = mb_substr($truncate, 0, $spacepos);
        }
    }
    $truncate .= $ending;

    if ($html) {
        foreach ($openTags as $tag) {
            $truncate .= '</'.$tag.'>';
        }
    }

    return $truncate;
}

/**
 * formats date time to system's format
 * @param type $dateTime
 * @return string
 */
function formatDateTime($dateTime)
{
    return date("j F Y g:i a",strtotime($dateTime));
}

/**
 * gets current milli secs
 * @return string
 */
function getCurrMilliSecs()
{
    return date("YmdHisu");
}

/**
 * upload image to server
 * @param string $filePost
 */
function uploadImage($filePost)
{
    $location = "";
    $pActualFile = $filePost;
    if(isset($_FILES[$pActualFile])){
        
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $tmp = explode(".", $_FILES[$pActualFile]["name"]);
        $extension = end($tmp);

        if((($_FILES[$pActualFile]["type"] == "image/gif")
            || ($_FILES[$pActualFile]["type"] == "image/jpeg")
            || ($_FILES[$pActualFile]["type"] == "image/jpg")
            || ($_FILES[$pActualFile]["type"] == "image/pjpeg")
            || ($_FILES[$pActualFile]["type"] == "image/png"))
            && ($_FILES[$pActualFile]["size"] < 10000000)
            && in_array(strtolower($extension), $allowedExts)){

            if($_FILES[$pActualFile]["error"] > 0){

//                echo "Return Code: " . $_FILES[$pActualFile]["error"] . "<br>";

            }else{

//                echo "Upload: " . $_FILES[$pActualFile]["name"] . "<br>";
//                echo "Type: " . $_FILES[$pActualFile]["type"] . "<br>";
//                echo "Size: " . ($_FILES[$pActualFile]["size"] / 1024) . " kB<br>";
//                echo "Temp file: " . $_FILES[$pActualFile]["tmp_name"] . "<br>";

                $fileName = trim(addslashes(getCurrMilliSecs()."_".$tmp[0].".".$extension));
                $fileName = str_replace(' ', '_', $fileName);
                $fileName = preg_replace('/\s+/', '_', $fileName);
                if (file_exists(ROOTPATH."/upload/profile/".$fileName)){

//                    echo $fileName . " already exists. ";
                    $location = "";
                }else{

                    move_uploaded_file($_FILES[$pActualFile]["tmp_name"],ROOTPATH."/upload/profile/".$fileName);
                    $location = "/upload/profile/".$fileName;

                }

            }

        }

    }
    
    return $location;
}