<?php

/* 
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    about.php
 * Date Created: Apr 11, 2015
 * Time Created: 3:49:50 PM
 */

require("common/include.php");
require(ROOTPATH."/class/template.php");
require(ROOTPATH."/class/user.php");

$template = new template();
$user = new user();
$template->header();
$B010 = $user->getUserList("0")[0];
$template->menu(); ?>
<div class="landing-banner"></div>
<br>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div style="width:150px;height:180px;background:url('<?=ROOTURI.$B010["B010ProfilePic"];?>') center center;background-size:cover;"></div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?=$B010["B010Nm"];?></h4>
                        <small><cite title="Singapore">Singapore <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-envelope"></i><?=$B010["B010Email"];?>
                            <br />
                            <i class="glyphicon glyphicon-globe"></i><a href="http://www.mervintan.com">www.mervintan.com</a>
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>Jan 30, 1989
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $template->scripts();?>

<?php $template->footer();