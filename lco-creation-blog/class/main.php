<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    main.php
 * Date Created: Mar 26, 2015
 * Time Created: 9:11:56 PM
 */

/**
 * Description of main
 *
 * @author mervintankw
 */
class main {
    
    public $db;
    
    /**
     * default constructor
     */
    public function __construct() 
    {
        $this->db = new db();
    }
    
}
