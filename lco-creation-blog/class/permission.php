<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    permission.php
 * Date Created: Mar 23, 2015
 * Time Created: 3:02:06 AM
 */

/**
 * This class manages permissions of users
 *
 * @author mervintankw
 */
include_once ROOTPATH.'/common/config.php';
include_once 'db.php';

class permission {
    
    public $db;
    public $UserCd;
    public $UserNm;
    public $Email;
    public $CompanyID;
    
    public function __construct()
    {
        if(!isset($_SESSION["UserCd"]))
        {
            if($_SERVER["SCRIPT_NAME"]!=="/view/login.php")
            {
                session_destroy();
                echo "<script>alert('Unauthorized access! Please login.');</script>";
                echo "<script>top.location.href='/view/login.php';</script>";
                exit;
            }
        }
        else
        {
            $this->UserCd       = $_SESSION["UserCd"];
            $this->UserNm       = $_SESSION["UserNm"];
            $this->Email        = $_SESSION["Email"];
            $this->CompanyID    = $_SESSION["CompanyID"];
        }
        $this->db = new db();
    }
    
}
