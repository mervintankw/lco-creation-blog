<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    html.php
 * Date Created: Mar 28, 2015
 * Time Created: 12:09:10 PM
 */

/**
 * Description of html
 *
 * @author mervintankw
 */
class html
{
    
    /**
     * default constructor
     */
    public function __construct(){}
    
    /**
     * generate html data table
     * @param string $tableId html id of table
     * @param array $headerData header data that goes into <th></th>
     * @param string $rowId row data-id=""
     * @param array $rowData row data
     * @param string $tableAttributes additional attributes for table
     * @param string $rowAttributes additional row attributes for rows
     * @param array $extraRowHeader extra row header
     * @param array $extraRowData extra row data e.g. class='btn btn-info' target='_blank'
     * @param array $extraRowText <a> tag text display
     * @param array $extraRowUrl <a> tag url with $rowId tagged at the back of the url
     */
    public function generateTable($tableId,$headerData=array(),$rowId="",$rowData=array(),$tableAttributes="",$rowAttributes="",$extraRowHeader=array(),$extraRowData=array(),$extraRowText=array(),$extraRowUrl=array())
    {
        if(!empty($rowData))
        {
            ?>
            <table id="<?=$tableId;?>" class='table table-responsive' <?=$tableAttributes;?> >
                <thead>
                    <tr>
                        <?php if(!empty($extraRowData)){ ?>
                            <?php foreach($extraRowHeader as $extraData){ ?>
                                <th><?=$extraData;?></th>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach($headerData as $index => $value){ ?>
                        <th><?=$index;?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rowData as $data){ ?>
                    <tr data-id="<?=$data->{$rowId};?>" <?=$rowAttributes;?> >
                        <?php if(!empty($extraRowData)){ ?>
                            <?php foreach($extraRowData as $index => $extraData){ ?>
                                <td><a href="<?=$extraRowUrl[$index].$data->{$rowId};?>" <?=$extraData;?> ><?=$extraRowText[$index];?></a></td>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach($headerData as $index => $value){ ?>
                        <td class="table-row"><?=$data->{$value};?></td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php
        }
    }
    
    /**
     * generate html bootstrap popup modal
     * @param string $modalId html id of the modal
     * @param string $modalTitle title of the modal
     * @param string $formAction html form action when modal form is submitted
     * @param string $modalMode modal mode e.g. entry
     * @param array $footerBtnArr (optional) any additional buttons to be displayed at the footer of the modal
     * @param boolean $footer indicates whether to show footer true for yes and false for no
     * @return array array["header"] & array["footer"]
     */
    public function generateModal($modalId,$modalTitle,$formAction,$modalMode,$footerBtnArr=array(),$footer=true)
    {
        ob_start(); ?>
        <!-- Modal -->
        <div class="modal fade" id="<?=$modalId;?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?=$modalTitle;?></h4>
                    </div>
                    <form role="form" id="newProjectForm" enctype="multipart/form-data" method="post" accept-charset="UTF-8" action="<?=$formAction;?>" target="" class="form-horizontal" >
                        <div class="modal-body">
                            <div class="main-content">
        <?php $modalHeader = ob_get_clean(); ?>
        <?php ob_start(); ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default <?=$footer?"":"hidden";?>" data-dismiss="modal">Cancel</button>
                            <!-- Display any additional buttons for modal here -->
                            <?php if(!empty($footerBtnArr)){foreach($footerBtnArr as $footerBtn){echo $footerBtn;}} ?>
                            <button type="submit" class="btn btn-success <?=$footer?"":"hidden";?>" name="<?=$modalMode;?>">Save changes</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php $modalFooter = ob_get_clean();
        return array(
            "header"=>$modalHeader,
            "footer"=>$modalFooter
        );
    }
    
    /**
     * generate html textarea
     * @param string $attributes
     * @param string $valueField
     */
    public function generateTextArea($attributes,$valueField="")
    {
        ?>
        <textarea <?=empty($attributes)?"":$attributes;?>><?=$valueField;?></textarea>
        <?php
    }
    
    /**
     * generate html input
     * @param string $attributes
     * @param string $valueField
     */
    public function generateInput($attributes,$valueField="")
    {
        ?>
        <input <?=empty($attributes)?"":$attributes;?> value="<?=$valueField;?>" />
        <?php
    }
    
    /**
     * generate html select
     * @param string $attributes e.g. id="select" class="form-control"
     * @param string $arrayType e.g. json/array
     * @param json $data e.g json/array
     * @param string $valueField option value attribute
     * @param string $textField option text attribute
     * @param string $defaultText default option field e.g. Select Company
     * @param string $selectedValue selected option value
     */
    public function generateSelect($attributes,$arrayType,$data,$valueField,$textField,$defaultText="",$selectedValue="")
    {
        $dataArr = array();
        if(!empty($data))
        {
            switch($arrayType)
            {
                case("json"):
                    foreach($data as $item)
                    {
                        $dataArr[$item->{$textField}] = $item->{$valueField};
                    }
                    break;
                case("array"):
                    foreach($data as $dataItem)
                    {
                        $dataArr[$dataItem[$textField]] = $dataItem[$valueField];
                    }
                    break;
                case("array-single"):
                    $dataArr = $data;
                    break;
            }
        }
        ?>
        <select <?=empty($attributes)?"":$attributes;?> >
            <option value=""><?=$defaultText;?></option>
        <?php foreach($dataArr as $vField => $tField){ ?>
            <option value="<?=$vField;?>" <?=$selectedValue==$vField?"selected":"";?>><?=$tField;?></option>
        <?php } ?>
        </select>
        <?php
    }
}
