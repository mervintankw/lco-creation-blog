<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    user.php
 * Date Created: Apr 5, 2015
 * Time Created: 3:03:38 PM
 */

/**
 * Description of user
 *
 * @author mervintankw
 */
//require "main.php";

class user extends main
{   
    /**
     * default constructor
     */
    public function __construct() 
    {
        parent::__construct(); 
    }
    
    /**
     * add user
     * @param string $B010Type
     * @param string $B010UserNm
     * @param string $B010Passwd
     * @param string $B010Nm
     * @param string $B010Email
     * @param string $B010Status
     * @param int $B010CreUserCd
     * @param int $B010ModUserCd
     * @return int
     */
    public function addUser($B010Type,$B010UserNm,$B010Passwd,$B010Nm,$B010Email,$B010ProfilePic,$B010Status,$B010CreUserCd,$B010ModUserCd)
    {
        $insertB010 = "insert into B010User set     B010Type       = ? ,
                                                    B010UserNm     = ? ,
                                                    B010Passwd     = ENCODE(?,?) ,
                                                    B010Nm         = ? ,
                                                    B010Email      = ? ,
                                                    B010ProfilePic = ? ,
                                                    B010Status     = ? ,
                                                    B010CreUserCd  = ? ,
                                                    B010ModUserCd  = ? ";
        $this->db->db_change_table($insertB010,"ssssssssii",array(
            $B010Type,
            $B010UserNm,
            $B010Passwd,
            SALT,
            $B010Nm,
            $B010Email,
            $B010ProfilePic,
            $B010Status,
            $B010CreUserCd,
            $B010ModUserCd
        ));
        return $this->db->db_insert_id();
    }
    
    /**
     * update user
     * @param int $B010UserCd
     * @param string $B010Type
     * @param string $B010UserNm
     * @param string $B010Passwd
     * @param string $B010Nm
     * @param string $B010Email
     * @param string $B010ProfilePic
     * @param string $B010Status
     * @param int $B010ModUserCd
     */
    public function updateUser($B010UserCd,$B010Type,$B010UserNm,$B010Passwd,$B010Nm,$B010Email,$B010ProfilePic,$B010Status,$B010ModUserCd)
    {
        $updateB010 = "update B010User set  B010Type       = ? ,
                                            B010UserNm     = ? ,";
        $updateB010 .= empty($B010Passwd)?"":"B010Passwd   = ENCODE(?,?) ,";
        $updateB010 .= "                    B010Nm         = ? ,
                                            B010Email      = ? ,
                                            B010ProfilePic = ? ,
                                            B010Status     = ? ,
                                            B010ModUserCd  = ?
                                    where   B010UserCd     = ? ";
        $typeStr = "";
        $valueArr = array();
        if(!empty($B010Passwd))
        {
            $typeStr = "ssssssssii";
            $valueArr = array(
                $B010Type,
                $B010UserNm,
                $B010Passwd,
                SALT,
                $B010Nm,
                $B010Email,
                $B010ProfilePic,
                $B010Status,
                $B010ModUserCd,
                $B010UserCd
            );
        }
        else
        {
            $typeStr = "ssssssii";
            $valueArr = array(
                $B010Type,
                $B010UserNm,
                $B010Nm,
                $B010Email,
                $B010ProfilePic,
                $B010Status,
                $B010ModUserCd,
                $B010UserCd
            );
        }
        $this->db->db_change_table($updateB010,$typeStr,$valueArr);
    }
    
    /**
     * delete user
     * @param int $B010UserCd
     */
    public function deleteUser($B010UserCd)
    {
        $deleteB010 = "delete from B010User where B010UserCd = ? ";
        $this->db->db_change_table($deleteB010,"i",array($B010UserCd));
    }
    
    /**
     * retrieve user list
     *
     * @param int $B010UserCd
     * @return array
     */
    public function getUserList($B010UserCd="")
    {
        if(empty($B010UserCd))
        {
            $qB010 = "select * from B010User ";
            return $this->db->db_query_table($qB010);
        }
        else
        {
            $qB010 = "select * from B010User where B010UserCd = ? ";
            return $this->db->db_query_table($qB010,"i",array($B010UserCd));
        }
    }
    
    /**
     * user login
     * @param string $UserNm
     * @param string $Passwd
     * @return array
     */
    public function login($UserNm,$Passwd)
    {
        $qB010 = "select * from B010User where B010UserNm	= ?
                                    and decode(B010Passwd,?) = ?
                                    and B010Status != 'Disabled' ";
        return $this->db->db_query_table($qB010,"sss",array($UserNm,SALT,$Passwd));
    }
    
    /**
     * user registration
     * @param string $UserType
     * @param string $UserNm
     * @param string $Passwd
     * @param string $Nm
     * @param string $Email
     * @param string $Status
     * @return array B010UserCd
     */
    public function register($UserType,$UserNm,$Passwd,$Nm,$Email,$Status)
    {
        $insertB010 = "insert into B010User set B010Type       = ? 
                                                B010UserNm     = ? ,
                                                B010Passwd     = ENCODE(?,?) ,
                                                B010Nm         = ? ,
                                                B010Email      = ? ,
                                                B010Status     = ? ,
                                                B010CreUserCd  = '1' ,
                                                B010CreDt      = now() ,
                                                B010ModUserCd  = '1' ";
        $this->db->db_change_table($insertB010,"sssssss",array($UserType,$UserNm,$Passwd,SALT,$Nm,$Email,$Status));
        return $this->db->db_insert_id();
    }
    
}
