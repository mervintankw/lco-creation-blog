<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    post.php
 * Date Created: Apr 6, 2015
 * Time Created: 9:55:08 PM
 */
//require "main.php";

/**
 * Description of post
 *
 * @author mervintankw
 */

class post extends main
{
    
    /**
     * default constructor
     */
    public function __construct() 
    {
        parent::__construct();
    }
   
    /**
     * get number of posts count
     * 
     * @return int
     */
    public function getNumOfPost()
    {
        $qB100 = "select count(*) as cnt from B100Post where B100Deleted <> 1 ";
        return $this->db->db_query_table($qB100);
    }
    
    /**
     * get archived posts
     * 
     * @param type $B100SeqNo
     * @return array
     */
    public function getArchivedPost($B100SeqNo="")
    {
        if(empty($B100SeqNo))
        {
            $qB100 = "select * from B100Post where B100Deleted = 1 ";
            return $this->db->db_query_table($qB100);
        }
        else
        {
            $qB100 = "select * from B100Post where B100Deleted = 1 and B100SeqNo = ? ";
            return $this->db->db_query_table($qB100,"i",array($B100SeqNo));
        }
    }
    
    /**
     * get posts by range
     * 
     * @param type $start
     * @param type $end
     * @return array
     */
    public function getPostByRange($start,$end)
    {
        $qB100 = "select * from B100Post where B100Deleted <> 1 order by B100CreDt desc limit ?,? ";
        return $this->db->db_query_table($qB100,"ii",array($start,$end));
    }
    
    /**
     * get posts
     * 
     * @param int $B100SeqNo
     * @return array
     */
    public function getPost($B100SeqNo="")
    {
        if(empty($B100SeqNo))
        {
            $qB100 = "select * from B100Post where B100Deleted <> 1 order by B100CreDt desc ";
            return $this->db->db_query_table($qB100);
        }
        else
        {
            $qB100 = "select * from B100Post where B100Deleted <> 1 and B100SeqNo = ? ";
            return $this->db->db_query_table($qB100,"i",array($B100SeqNo));
        }
    }
    
    /**
     * insert post
     * 
     * @param string $B100Type
     * @param string $B100Title
     * @param string $B100Content
     * @return B100SeqNo
     */
    public function insertPost($B100Type,$B100Title,$B100Content)
    {
        $insertB100 = "insert into B100Post set B100Type       = ? ,
                                                B100Title      = ? ,
                                                B100Content    = ? ,
                                                B100CreUserCd  = ? ,
                                                B100CreDt      = now() ,
                                                B100ModUserCd  = ? ";
        $this->db->db_change_table($insertB100,"sssii",array($B100Type,$B100Title,$B100Content,$_SESSION["UserCd"],$_SESSION["UserCd"]));
        return $this->db->db_insert_id();
    }
    
    /**
     * update post
     * 
     * @param int $B100SeqNo
     * @param string $B100Type
     * @param string $B100Title
     * @param string $B100Content
     */
    public function updatePost($B100SeqNo,$B100Type,$B100Title,$B100Content)
    {
        $updateB100 = "update B100Post set  B100Type       = ? ,
                                            B100Title      = ? ,
                                            B100Content    = ? ,
                                            B100ModUserCd  = ?
                                    where   B100SeqNo      = ? ";
        $this->db->db_change_table($updateB100,"sssii",array($B100Type,$B100Title,$B100Content,$_SESSION["UserCd"],$B100SeqNo));
    }
    
    /**
     * archive post
     * 
     * @param int $B100SeqNo
     */
    public function archivePost($B100SeqNo)
    {
        $updateB010 = "update B100Post set B100Deleted = 1 where B100SeqNo = ? ";
        $this->db->db_change_table($updateB010,"i",array($B100SeqNo));
    }
    
    /**
     * delete post
     * 
     * @param int $B100SeqNo
     */
    public function deletePost($B100SeqNo)
    {
        $deleteB100 = "delete from B100Post where B100SeqNo = ? ";
        $this->db->db_change_table($deleteB100,"i",array($B100SeqNo));
    }
    
    /**
     * restore post from archive
     * 
     * @param int $B100SeqNo
     */
    public function restorePost($B100SeqNo="")
    {
        $restoreB100 = "update B100Post set B100Deleted = 0 where B100SeqNo = ? ";
        $this->db->db_change_table($restoreB100,"i",array($B100SeqNo));
    }
    
    /**
     * get number of posts per month for each year
     * 
     * @return array
     */
    public function getPostStats()
    {
        $qB100 = "select date_format(B100CreDt, '%M, %Y') as monthYear, count(*) as numPerMonth from B100Post group by monthYear order by B100CreDt desc ";
        return $this->db->db_query_table($qB100);
    }
    
}
