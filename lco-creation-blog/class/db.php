<?php

/**
 * Description of db
 *
 * @author mervintankw
 */
class db {
    
    public $mysqli;
    public $last_insert_id;
    public $num_of_rows;
    
    function __construct()
    {
        $this->db_connect();
    }
    
    /**
     * Connect to db function
     */
    public function db_connect()
    {
        $this->mysqli = new mysqli(DB_SERVER_URL, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_SERVER_PORT);
        if ($this->mysqli->connect_errno) {
            echo "Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
        }
    }
    
    /**
     * function that runs insert,update,delete
     * @param string $sql
     * @param string $bind_types
     * @param array $bind_values
     */
    public function db_change_table($sql,$bind_types=null,$bind_values=null)
    {
        # create a prepared statement
        $stmt = $this->mysqli->prepare($sql);

        if($bind_types&&$bind_values)
        {
            $bind_names[] = $bind_types;
            for ($i=0; $i<count($bind_values);$i++) 
            {
                $bind_name = 'bind' . $i;
                $$bind_name = $bind_values[$i];
                $bind_names[] = &$$bind_name;
            }
            call_user_func_array(array($stmt,'bind_param'),$bind_names);
        }

        # execute query 
        $stmt->execute();
        
        # store last insert id
        $this->last_insert_id = $stmt->insert_id;
        
        # close statement
        $stmt->close();
    }
    
    /**
     * function that query table
     * @param type $sql
     * @param type $bind_types
     * @param type $bind_values
     * @return array
     */
    public function db_query_table($sql,$bind_types=null,$bind_values=null)
    {
        # create a prepared statement
        $stmt = $this->mysqli->prepare($sql);
        
        if($bind_types&&$bind_values)
        {
            $bind_names[] = $bind_types;
            for ($i=0; $i<count($bind_values);$i++) 
            {
                $bind_name = 'bind' . $i;
                $$bind_name = $bind_values[$i];
                $bind_names[] = &$$bind_name;
            }
            $return = call_user_func_array(array($stmt,'bind_param'),$bind_names);
        }

        # execute query 
        $stmt->execute();

        # store query results
        $stmt->store_result();
        
        # store number of rows
        $this->num_of_rows = $stmt->num_rows;
        
        # these lines of code below return one dimentional array, similar to mysqli::fetch_assoc()
        $meta = $stmt->result_metadata(); 

        while ($field = $meta->fetch_field()) 
        { 
            $var = $field->name; 
            $$var = null; 
            $parameters[$field->name] = &$$var;
        }

        call_user_func_array(array($stmt, 'bind_result'), $parameters);
        
        // returns a copy of a value
        $copy = create_function('$a', 'return $a;');
        
        $returnArr = array();
        while($stmt->fetch()) 
        { 
            $returnArr[] = array_map($copy,$parameters);
        }

        # close statement
        $stmt->close();
        
        return $returnArr;
    }
    
    /**
     * returns last insert id
     * @return last_insert_id
     */
    public function db_insert_id()
    {
        return $this->last_insert_id;
    }
    
    /**
     * returns number of rows
     * @return num_of_rows
     */
    public function db_num_of_rows()
    {
        return $this->num_of_rows;
    }
}
