<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    template.php
 * Date Created: Mar 23, 2015
 * Time Created: 3:01:00 AM
 */

/**
 * template class manages header and footer contents
 *
 * @author mervintankw
 */
include_once 'syscon.php';

class template extends main {
    
    public $syscon;
    
    /**
     * Default constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->syscon = new syscon();
    }
    
    public function menu()
    {
        $aHeaderMenu = $this->syscon->getHeaderMenu();
        ?>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?=ROOTURI;?>/index.php" style="padding-left:0px;margin-top:-11px;">
                            <img src="<?=ROOTURI;?>/img/bt_logo.png" style="width:auto;height:62px;" alt=""/>
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php foreach($aHeaderMenu as $headerMenu){
                                if($headerMenu["Zsy99Key3"]=="Main" && $headerMenu["Zsy99Descp1"]==""){ 
                                    $aSubMenu = $this->syscon->getSubMenu($headerMenu["Zsy99Key1"]); ?>
                                    <li class="dropdown <?=SCRIPT_NAME==$headerMenu["Zsy99Key1"]?"active":"";?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?=$headerMenu["Zsy99Key1"];?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php foreach($aSubMenu as $subMenu){ ?>
                                            <li><a href="<?=$subMenu["Zsy99Descp1"];?>"><?=$subMenu["Zsy99Key2"];?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php }else{ ?>
                                    <li class="<?=SCRIPT_NAME==$headerMenu["Zsy99Key1"]?"active":"";?>"><a href="<?=ROOTURI.$headerMenu["Zsy99Descp1"];?>"><?=$headerMenu["Zsy99Key1"];?></a></li>
                            <?php }
                            } ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                        <?php if(isset($_SESSION["UserCd"])){ ?>
                            <li><a href="<?=ROOTURI;?>/view/logout.php"><i class="fa fa-sign-out fa-1x"></i> Log out</a></li>
                        <?php }else{ ?>
                            <li><a href="<?=ROOTURI;?>/view/login.php"><i class="fa fa-sign-in fa-1x"></i> Login</a></li>
                        <?php } ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </div>
        </nav>
        <?php
    }
    
    /**
     * Header function
     */
    public function header()
    {
        ?>
        <html>
            <head>
                <!-- Bootstrap css -->
                <link href="<?=ROOTURI;?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
                <!-- Awesome fonts css -->
                <link href="<?=ROOTURI;?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
                <!-- FSS custom css -->
                <link href="<?=ROOTURI;?>/css/style.css" rel="stylesheet" type="text/css"/>
                <!-- Data table css -->
                <link href="<?=ROOTURI;?>/vendor/datatables/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
                <!-- Google fonts -->
                <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:700italic,700,300,400' rel='stylesheet' type='text/css'>
                <!-- Prettify -->
                <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
                <!-- Morris -->
                <link href="<?=ROOTURI;?>/css/morris.css" rel="stylesheet" type="text/css"/>
                
            </head>
            <body>
        <?php
    }
    
    /**
     * Load scripts function
     */
    public function scripts()
    {
        ?>
            <!-- jQuery -->
            <script src="<?=ROOTURI;?>/js/jquery-2.1.3.min.js" type="text/javascript"></script>
            <!-- Boostrap js -->
            <script src="<?=ROOTURI;?>/js/bootstrap.min.js" type="text/javascript"></script>
            <!-- Pinterest grid js -->
            <script src="<?=ROOTURI;?>/js/pinterest_grid.js" type="text/javascript"></script>
            <!-- Data table js -->
            <script src="<?=ROOTURI;?>/vendor/datatables/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
            <!-- Tinymce js -->
            <script src="<?=ROOTURI;?>/vendor/tinymce/tinymce/tinymce.min.js" type="text/javascript"></script>
            <!-- Raphael js -->
            <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
            <!-- Morris js -->
            <script src="<?=ROOTURI;?>/js/morris.min.js" type="text/javascript"></script>
            <!-- Prettify js -->
            <script src="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
        <?php
    }
    
    /**
     * Footer function
     */
    public function footer()
    {
        ?>
            </body>
        </html>   
        <?php
    }
}
