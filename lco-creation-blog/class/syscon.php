<?php

/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    syscon.php
 * Date Created: Mar 16, 2015
 * Time Created: 1:22:00 AM
 */

/**
 * System Controller Documentation
 * 001: Read/Write/None permission constants
 * 002: Menu Items
 */

/**
 * Description of syscon
 *
 * @author mervintankw
 */
class syscon extends main {
    
    /**
     * default constructor
     */
    public function __construct() 
    {
        parent::__construct();
    }
    
    /**
     * get user types
     * 
     * @return array
     */
    public function getUserType()
    {
        $qZ999 = "select * from Z999SysCon where Zsy99No = '003' ";
        return $this->db->db_query_table($qZ999);
    }
    
    /**
     * get all menu
     */
    public function getMenu()
    {
        if(isset($_SESSION["UserCd"]))
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key2 = 'LoggedIn' order by Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
        else
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key2 = '' order by Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
    }
    
    /**
     * get header menu
     */
    public function getHeaderMenu()
    {
        if(isset($_SESSION["UserCd"]))
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key3 = 'Main' and Zsy99Key2 = 'LoggedIn' order by Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
        else
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key3 = 'Main' and Zsy99Key2 = '' order by Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
    }
    
    /**
     * get sub menu
     * @param string $Zsy99Key1 header menu name
     */
    public function getSubMenu($Zsy99Key1="")
    {
        if(isset($_SESSION["UserCd"]))
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key3 = 'Sub' and Zsy99Key2 = 'LoggedIn' order by Zsy99Key1,Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
        else
        {
            $qZ999 = "select * from Z999SysCon where Zsy99No = '002' and Zsy99Key3 = 'Sub' and Zsy99Key2 = '' order by Zsy99Key1,Zsy99Set1 ";
            return $this->db->db_query_table($qZ999);
        }
    }
    
    /**
     * insert menu
     * @param string $Zsy99Key1 new menu name
     * @param string $Zsy99Key2 new menu sub name
     * @param string $Zsy99Key3 menu type e.g. Main/Sub
     * @param string $Zsy99Set1 new menu ordering number
     * @param string $Zsy99Descp1 new menu url
     */
//    public function insertMenu($Zsy99Key1,$Zsy99Key2,$Zsy99Key3,$Zsy99Set1,$Zsy99Descp1)
//    {
//        $insertZ999 = "insert into Z999SysCon set   Zsy99No = '002',
//                                                    Zsy99Key1 = ?,
//                                                    Zsy99Key2 = ?,
//                                                    Zsy99Key3 = ?,
//                                                    Zsy99Set1 = ?,
//                                                    Zsy99Descp1 = ?,
//                                                    Zsy99CreDt = now() ";
//        $this->db->db_change_table($insertZ999,"sssss",array($Zsy99Key1,$Zsy99Key2,$Zsy99Key3,$Zsy99Set1,$Zsy99Descp1));
//        $this->setApiResponse("200","Menu inserted",array("Zsy99No"=>$this->db->db_insert_id()));
//    }
    
    /**
     * update menu
     * @param string $Zsy99Key1 new menu name
     * @param string $Zsy99Key2 new menu sub name
     * @param string $Zsy99Key3 menu type e.g. Main/Sub
     * @param string $Zsy99Set1 new menu ordering number
     * @param string $Zsy99Descp1 new menu url
     * @param string $Zsy99Key1Hd old menu name
     * @param string $Zsy99Key2Hd old menu sub name
     * @param string $Zsy99Set1Hd old menu ordering number
     * @param string $Zsy99Descp1Hd old menu url
     */
//    public function updateMenu($Zsy99Key1,$Zsy99Key2,$Zsy99Key3,$Zsy99Set1,$Zsy99Descp1,$Zsy99Key1Hd,$Zsy99Key2Hd,$Zsy99Set1Hd,$Zsy99Descp1Hd)
//    {
//        $updateZ999 = "update Z999SysCon set Zsy99Key1 = ?,Zsy99Key2 = ?,Zsy99Set1 = ?,Zsy99Descp1 = ?
//                                         where  Zsy99No = '002'
//                                         and    Zsy99Key1 = ?
//                                         and    Zsy99Key2 = ?
//                                         and    Zsy99Key3 = ?
//                                         and    Zsy99Set1 = ?
//                                         and    Zsy99Descp1 = ?";
//        $this->db->db_change_table($updateZ999,"sssssssss",array($Zsy99Key1,$Zsy99Key2,$Zsy99Set1,$Zsy99Descp1,$Zsy99Key1Hd,$Zsy99Key2Hd,$Zsy99Key3,$Zsy99Set1Hd,$Zsy99Descp1Hd));
//        $this->setApiResponse("200","Menu updated","");
//    }
    
    /**
     * delete menu
     * @param string $Zsy99Key1 menu name
     * @param string $Zsy99Key3 menu type e.g. Main/Sub
     * @param string $Zsy99Descp1 menu url
     */
//    public function deleteMenu($Zsy99Key1,$Zsy99Key3,$Zsy99Descp1)
//    {
//        $deleteZ999 = "delete from Z999SysCon where Zsy99Key1 = ? and Zsy99Key3 = ? and Zsy99Descp1 = ? and Zsy99No = '002' ";
//        $this->db->db_change_table($deleteZ999,"sss",array($Zsy99Key1,$Zsy99Key3,$Zsy99Descp1));
//        $this->setApiResponse("200","Menu deleted","");
//    }
    
    /**
     * generic get sysem controller function
     * @param string $returnAs return as num/array
     * @param array $additionalQuery additional query e.g. " where 1 "
     * @return array/int return value
     */
//    public function getSyscon($returnAs,$additionalQuery="")
//    {
//        $qZ999 = "select * from Z999SysCon ";
//        $qZ999 .= (!empty($additionalQuery))?$additionalQuery:"";
//        $aZ999 = $this->db->db_query_table($qZ999);
//        switch($returnAs)
//        {
//            case("num"):
//                return $this->db->db_num_of_rows();
//            case("array"):
//                return $aZ999;
//        }
//    }
}

