<?php
/*
 * Copyright (C) Mervin Tan Kok Wei from 2014 to Present
 * All Rights Reserved
 * www.mervintan.com
 * 
 * File Author:  mervintankw
 * File Name:    index.php
 * Date Created: Mar 11, 2015
 * Time Created: 8:49:22 PM
 */

require("common/include.php");
require(ROOTPATH."/class/template.php");
require(ROOTPATH."/class/post.php");
$template = new template();
$template->header();
$post = new post();
$html = new html();
$aB100 = $post->getPostByRange(0,LOAD_NUM_OF_POSTS);
?> 
<?php $template->menu();?>
<div class="landing-banner"></div>
<img id='loading' src='<?=ROOTURI;?>/img/loading.gif'>
<div class="container" style="padding-top:30px;">
    <section id="blogPostSection">
    <?php foreach($aB100 as $B100){ ?>
        <article class="white-panel panel panel-default">
            <div class="panel-body">
            <h4><?=$B100["B100Title"];?></h4>
            <p><?=truncate($B100["B100Content"],200,array("html"=>true,"ending"=>' <a style="cursor: pointer;" onclick="readMore(\''.$B100["B100SeqNo"].'\');">Read More</a>'));?></p>
            </div>
            <div class="panel-footer text-right">
                    <?=formatDateTime($B100["B100CreDt"]);?>
            </div>
        </article>
    <?php } ?>
    </section>

<?=$html->generateInput(" id='currCount' name='currCount' type='hidden' ",LOAD_NUM_OF_POSTS);?>
<?=$html->generateInput(" id='numOfPost' name='numOfPost' type='hidden' ",$post->getNumOfPost()[0]["cnt"]);?>
<?=$html->generateInput(" id='finishLoad' name='numOfPost' type='hidden' ",0);?>

<div id="ajaxPanel"></div>

    <div class="row form-group hidden">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">You can even have a Panel Title</h3>
                </div>
                <div class="panel-image hide-panel-body">
                    <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52c470899a2e1_1.JPG" class="panel-image-preview" />
                    <label for="toggle-3"></label>
                </div>
                <input type="checkbox" id="toggle-3" class="panel-image-toggle">
                <div class="panel-body">
                    <h4>Title of Image</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in lobortis nisl, vitae iaculis sapien. Phasellus ultrices gravida massa luctus ornare. Suspendisse blandit quam elit, eu imperdiet neque semper et.</p>
                </div>
                <div class="panel-footer text-center">
                    <a href="#download"><span class="glyphicon glyphicon-download"></span></a>
                    <a href="#facebook"><span class="fa fa-facebook"></span></a>
                    <a href="#twitter"><span class="fa fa-twitter"></span></a>
                    <a href="#share"><span class="glyphicon glyphicon-share-alt"></span></a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-image">
                    <img src="http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_52cd36aac6bed_1.JPG" class="panel-image-preview" />
                    <label for="toggle-4"></label>
                </div>
                <input type="checkbox" id="toggle-4" checked class="panel-image-toggle">
                <div class="panel-body">
                    <h4>Show the Description by default!</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in lobortis nisl, vitae iaculis sapien. Phasellus ultrices gravida massa luctus ornare. Suspendisse blandit quam elit, eu imperdiet neque semper et.</p>
                </div>
                <div class="panel-footer text-center">
                    <a href="#download"><span class="glyphicon glyphicon-download"></span></a>
                    <a href="#facebook"><span class="fa fa-facebook"></span></a>
                    <a href="#twitter"><span class="fa fa-twitter"></span></a>
                    <a href="#share"><span class="glyphicon glyphicon-share-alt"></span></a>
                </div>
            </div>
        </div>
	</div>
</div>
<?php $template->scripts();?>
<script>
    /**
     * populate modal with blog post based on post id and display modal
     * @param int B100SeqNo
     */
    function readMore(B100SeqNo)
    {
        $.post("<?=ROOTURI;?>/ajax/post.php",
        {
            mode:"readMore",
            id:B100SeqNo
        })
        .done(function(data,status) {
            $("#ajaxPanel").html( data );
            $('#readMoreModal').modal('toggle');
         })
        .fail(function(data,status) {
            alert(JSON.stringify(data));
         });
    }
    
    $(document).ready(function() {
        /**
         * initialize pinterest grid js
         */
        $('#blogPostSection').pinterest_grid({
            no_columns: 2,
            padding_x: 10,
            padding_y: 10,
            margin_bottom: 50,
            single_column_breakpoint: 700
        });
    });
    
    /**
     * trigger load more blog posts script when user scrolls to the bottom of the page
     */
    window.onscroll = function(ev) {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            $("#loading").show();
            
            var currCount = parseInt($("#currCount").val());
            var totalCount = parseInt($("#numOfPost").val());
            var updatedCount = currCount+parseInt("<?=LOAD_NUM_OF_POSTS;?>");
            
            // checks if end pull is greater than total number of posts and if so use total num of posts as end point
            if(currCount <= totalCount)
            {
                // update current count
                $("#currCount").val("");
                $("#currCount").val(updatedCount);

//                alert("new range: "+currCount+" to "+updatedCount);

                $.post("<?=ROOTURI;?>/ajax/post.php",
                {
                    mode:"loadMore",
                    start:updatedCount,
                    end:<?=LOAD_NUM_OF_POSTS;?>
                })
                .done(function(data,status) {
                    // append new posts to the bottom of the page
                    $("#blogPostSection").append(data);
                    // reinitialize pinterest grid js
                    $('#blogPostSection').pinterest_grid({
                        no_columns: 2,
                        padding_x: 10,
                        padding_y: 10,
                        margin_bottom: 50,
                        single_column_breakpoint: 700
                    });
                 })
                .fail(function(data,status) {
                    alert(JSON.stringify(data));
                 });
            }
            else
            {
                var finishLoad = parseInt($("#finishLoad").val());
                if(finishLoad == 0)
                {
                    $("#finishLoad").val("");
                    $("#finishLoad").val(1);
                    
                    currCount = currCount - parseInt(<?=LOAD_NUM_OF_POSTS;?>);
                    
                    var numToPull = currCount - totalCount;
                
                    // update current count
                    $("#currCount").val("");
                    $("#currCount").val(currCount);

                    $.post("<?=ROOTURI;?>/ajax/post.php",
                    {
                        mode:"loadMore",
                        start:currCount,
                        end:numToPull
                    })
                    .done(function(data,status) {
                        // append new posts to the bottom of the page
                        $("#blogPostSection").append(data);
                        // reinitialize pinterest grid js
                        $('#blogPostSection').pinterest_grid({
                            no_columns: 2,
                            padding_x: 10,
                            padding_y: 10,
                            margin_bottom: 50,
                            single_column_breakpoint: 700
                        });
                     })
                    .fail(function(data,status) {
                        alert(JSON.stringify(data));
                     });
                }
            }
            $("#loading").hide();
        }
    };

</script>
<?php $template->footer();